-module(copper_cowboy_bridge).

-export([init/2, start_link/2, read_entire_body/1]).

-include("../gen/src/copper@final_response_FinalResponse.hrl").

-define(NUM_ACCEPTORS, 100).
-define(MAX_CONNECTIONS, 16384).

% plug cowboy code: https://github.com/elixir-plug/plug_cowboy/blob/v2.4.1/lib/plug/cowboy.ex#L1

start_link(Port, {Handler, Config, InitResult}) ->
    Dispatch = cowboy_router:compile([{'_', [], [
     {"/assets/[...]", cowboy_static, {priv_dir, gleam_todo,  "static/assets/"}},
     {'_', [], ?MODULE, {Handler, Config, InitResult}}
    ]}]),
    
    RanchOptions = #{
        max_connections => ?MAX_CONNECTIONS,
        num_acceptors => ?NUM_ACCEPTORS,
        socket_opts => [{port, Port}]
    },
    CowboyOptions = #{
        env => #{dispatch => Dispatch},
        stream_handlers => [cowboy_stream_h]
    },
    ranch_listener_sup:start_link(
        {gleam_cowboy, make_ref()},
        ranch_tcp, RanchOptions,
        cowboy_clear, CowboyOptions
    ).

init(Req, {Handler, Config, InitResult}) ->
    Resp = Handler(Req, Config, InitResult),
    Body = Resp#final_response.body,
    StatusCode = Resp#final_response.status_code,
    Headers = Resp#final_response.headers,
    Cookies = Resp#final_response.cookies,
    ReqWithCookies= maps:fold(fun(K,V,Acc) -> cowboy_req:set_resp_cookie(K,V, Acc) end, Req, Cookies),
    Reply = cowboy_req:reply(StatusCode, Headers, Body, ReqWithCookies),
    {ok, Reply, {Handler, Config, InitResult}}.
  
read_entire_body(Req) ->
    read_entire_body([], Req).

read_entire_body(Body, Req0) ->
    case cowboy_req:read_body(Req0) of
        {ok, Chunk, Req1} -> {list_to_binary([Body, Chunk]), Req1};
        {more, Chunk, Req1} -> read_entire_body([Body, Chunk], Req1)
    end.

