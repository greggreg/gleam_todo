// TODO
// Limit things like Type to specifics?
import gleam/list
import gleam/string

pub type FormMethod {
  FormGet
  FormPost
}

pub type Attribute {
  Action(String)
  Alt(String)
  Autocomplete(String)
  Autofocus(String)
  Class(String)
  Charset(String)
  Checked(Bool)
  Href(String)
  Id(String)
  Lang(String)
  Method(FormMethod)
  Name(String)
  Placeholder(String)
  Rel(String)
  Src(String)
  Type(String)
  Value(String)
}

pub fn class_list(classes: List(tuple(String, Bool))) -> Attribute {
  classes
  |> list.filter_map(fn(x) {
    case x {
      tuple(c, True) -> Ok(c)
      _ -> Error(Nil)
    }
  })
  |> string.join(" ")
  |> Class
}

pub fn render(a: List(Attribute)) -> String {
  let attrs =
    a
    |> list.map(render_attribute)
    |> string.join(" ")
  case attrs {
    "" -> ""
    _ -> string.concat([" ", attrs, " "])
  }
}

fn render_attribute(a: Attribute) -> String {
  let key_val = case a {
    Action(a) -> tuple("action", a)
    Alt(a) -> tuple("alt", a)
    Autocomplete(a) -> tuple("autocomplete", a)
    Autofocus(a) -> tuple("autofocus", a)
    Class(c) -> tuple("class", c)
    Charset(c) -> tuple("charset", c)
    Checked(True) -> tuple("checked", "")
    Checked(False) -> tuple("", "")
    Href(href) -> tuple("href", href)
    Id(id) -> tuple("id", id)
    Lang(l) -> tuple("lang", l)
    Method(FormGet) -> tuple("method", "get")
    Method(FormPost) -> tuple("method", "post")
    Name(n) -> tuple("name", n)
    Placeholder(p) -> tuple("placeholder", p)
    Rel(rel) -> tuple("rel", rel)
    Src(rel) -> tuple("src", rel)
    Type(t) -> tuple("type", t)
    Value(v) -> tuple("value", v)
  }

  case key_val {
    tuple("", _) -> ""
    tuple(key, val) ->
      [key, "=", "\"", val, "\""]
      |> string.join("")
  }
}
