////
//// Adapter for using Cowoboy with Copper
////

// TODO:
// configure static assets
import copper
import copper/connection
import copper/final_response.{FinalResponse}
import gleam/atom.{Atom}
import gleam/dynamic.{Dynamic}
import gleam/map.{Map}
import gleam/otp/process.{Pid}

pub external type CowboyRequest

pub fn server_interface() -> connection.ServerInterface(CowboyRequest) {
  connection.ServerInterface(
    get_request_cookies: cowboy_request_get_cookies,
    get_request_path: cowboy_request_get_path,
    get_request_query: cowboy_request_get_query,
    get_request_scheme: cowboy_request_get_scheme,
    get_request_method: cowboy_request_get_method,
    get_request_headers: cowboy_request_get_headers,
    get_request_string_body: cowboy_request_get_string_body,
    get_request_url_encoded_body: cowboy_request_get_url_encoded_body,
  )
}

// Cowboy Interface
pub external fn cowboy_start_link(
  port: Int,
  handler: tuple(
    fn(raw_request, copper.Config(init_result, raw_request), init_result) ->
      FinalResponse,
    copper.Config(init_result, raw_request),
    init_result,
  ),
) -> Result(Pid, Dynamic) =
  "copper_cowboy_bridge" "start_link"

external fn cowboy_request_get_cookies(
  req: CowboyRequest,
) -> List(tuple(String, String)) =
  "cowboy_req" "parse_cookies"

external fn cowboy_request_get_method(req: CowboyRequest) -> String =
  "cowboy_req" "method"

external fn cowboy_request_get_path(req: CowboyRequest) -> String =
  "cowboy_req" "path"

external fn cowboy_request_get_query(req: CowboyRequest) -> String =
  "cowboy_req" "qs"

external fn cowboy_request_get_scheme(req: CowboyRequest) -> String =
  "cowboy_req" "scheme"

external fn cowboy_request_get_headers(
  req: CowboyRequest,
) -> Map(String, String) =
  "cowboy_req" "headers"

external fn cowboy_request_get_string_body(
  req: CowboyRequest,
) -> tuple(Atom, String, CowboyRequest) =
  "cowboy_req" "read_body"

// TODO: the values returned can be booleans as well as strings
// will need a bridge function to handle this.
external fn cowboy_request_get_url_encoded_body(
  req: CowboyRequest,
) -> tuple(Atom, List(tuple(String, String)), CowboyRequest) =
  "cowboy_req" "read_urlencoded_body"
