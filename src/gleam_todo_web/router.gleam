import copper
import copper/connection.{Connection, Get, Post}
import copper/headers
import copper/mime_types
import copper/status_code
import gleam/io
import gleam/map
import gleam/option.{None}
import gleam/otp/process.{Sender}
import gleam/result
import gleam/string
import gleam_html as html
import gleam_todo/todo_store
import gleam_todo_web/constants.{session_id_key}
import gleam_todo_web/routes as r
import gleam_todo_web/todo_controller
import gleam_uuid

// This is the root connection pipeline
// It is responsible for taking the new connection and passing it around the application
pub fn pipeline(
  conn: Connection(a),
  store: Sender(todo_store.Message),
) -> Connection(a) {
  [
    connection.load(_, [connection.Path, connection.Method, connection.Headers]),
    ensure_session_id(_),
    dispatch(_, store),
  ]
  |> copper.pipe_thru(conn)
}

// This is the path -> controller dispatch
// what is traditionally considered a router
pub fn dispatch(
  conn: Connection(a),
  store: Sender(todo_store.Message),
) -> Connection(a) {
  case connection.get_data(conn, session_id_key) {
    Ok(session_id) -> {
      let route =
        conn
        |> connection.get_request_path()
        |> r.from_path()
      let method = connection.get_request_method(conn)
      let deps = todo_controller.Deps(store: store, session_id: session_id)
      ensure_todos_for_session(store, session_id)
      case tuple(method, route) {
        tuple(Get, r.AllTodos) -> todo_controller.all(conn, deps)
        tuple(Get, r.ActiveTodos) -> todo_controller.active(conn, deps)
        tuple(Get, r.CompleteTodos) -> todo_controller.complete(conn, deps)
        tuple(Get, r.EditTodo(id)) -> todo_controller.edit(conn, deps, id)
        tuple(Post, r.CreateTodo) -> todo_controller.create(conn, deps)
        tuple(Post, r.DeleteCompletedTodos) ->
          todo_controller.delete_completed(conn, deps)
        tuple(Post, r.DeleteTodo(id)) -> todo_controller.delete(conn, deps, id)
        tuple(Post, r.EditTodo(id)) -> todo_controller.do_edit(conn, deps, id)
        tuple(Post, r.MarkActive(id)) ->
          todo_controller.mark_active(conn, deps, id)
        tuple(Post, r.MarkComplete(id)) ->
          todo_controller.mark_complete(conn, deps, id)
        _ ->
          // TODO: probably just redirect back home
          conn
          |> connection.put_response_status_code(status_code.NotFound)
          |> connection.put_response_body("thats a 404")
      }
      |> default_to_200()
    }
    Error(_) ->
      conn
      |> connection.put_response_status_code(status_code.BadRequest)
      |> connection.put_response_body("no session id")
  }
}

// Ensure that for any session there are some todos
fn ensure_todos_for_session(
  store: Sender(todo_store.Message),
  session_id: String,
) -> Bool {
  process.call(
    store,
    fn(f) { todo_store.EnsureTodos(from: f, session_id: session_id) },
    1000,
  )
}

// Set a status code if there isn't one set
fn default_to_200(conn: Connection(a)) -> Connection(a) {
  case connection.get_response_status_code(conn) {
    None -> connection.put_response_status_code(conn, status_code.OK)
    _ -> conn
  }
}

// If a client doesn't have a valid session, give them one
fn ensure_session_id(conn: Connection(a)) -> Connection(a) {
  let session_id =
    conn
    |> connection.get_data_key(session_id_key)
    |> fn(s) {
      case s {
        Ok(_) -> s
        _ ->
          conn
          |> connection.get_request_cookies()
          |> map.get(session_id_key)
      }
    }
    |> result.then(gleam_uuid.from_string)
    |> fn(r) {
      case r {
        Ok(x) -> x
        _ -> gleam_uuid.v4()
      }
    }
    |> gleam_uuid.to_string()

  conn
  |> connection.put_response_cookie(session_id_key, session_id)
  |> connection.put_data(session_id_key, session_id)
}
