import gleam/list
import gleam/string

pub type Route {
  AllTodos
  ActiveTodos
  CompleteTodos
  CreateTodo
  DeleteCompletedTodos
  DeleteTodo(String)
  EditTodo(String)
  MarkActive(String)
  MarkComplete(String)
  NotFound
}

pub fn to_path(route: Route) -> String {
  [
    "/",
    case route {
      AllTodos -> []
      ActiveTodos -> ["active"]
      CompleteTodos -> ["complete"]
      CreateTodo -> ["create"]
      DeleteTodo(id) -> ["delete", id]
      DeleteCompletedTodos -> ["clear-completed"]
      EditTodo(id) -> ["edit", id]
      MarkActive(id) -> ["mark", "active", id]
      MarkComplete(id) -> ["mark", "complete", id]
      NotFound -> ["404"]
    }
    |> string.join("/"),
  ]
  |> string.concat()
}

pub fn from_path(path: String) -> Route {
  let path_parts =
    path
    |> string.split("/")
    |> list.filter(fn(p) { p != "" })
  case path_parts {
    [] -> AllTodos
    ["active"] -> ActiveTodos
    ["complete"] -> CompleteTodos
    ["clear-completed"] -> DeleteCompletedTodos
    ["create"] -> CreateTodo
    ["delete", id] -> DeleteTodo(id)
    ["edit", id] -> EditTodo(id)
    ["mark", "active", id] -> MarkActive(id)
    ["mark", "complete", id] -> MarkComplete(id)
    _ -> NotFound
  }
}
