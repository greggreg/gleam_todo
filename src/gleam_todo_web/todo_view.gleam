import gleam/bool
import gleam/int
import gleam/list
import gleam/string
import gleam_html.{HTML} as html
import gleam_html/attributes as ha
import gleam_todo/todo_store.{Todo}
import gleam_todo_web/routes as routes

fn css_path(name: String) -> String {
  ["/assets/", name, ".css"]
  |> string.concat()
}

fn image_path(name: String) -> String {
  ["/assets/", name]
  |> string.concat()
}

pub fn home(
  todos: List(Todo),
  todos_route: routes.Route,
  editing: Result(String, Nil),
) -> HTML {
  let num_todos =
    todos
    |> list.length()

  let num_completed =
    todos
    |> list.filter(fn(t: Todo) { t.complete })
    |> list.length()

  let new_input_base_attrs = [
    ha.Class("new-todo"),
    ha.Placeholder("What needs to be complete?"),
    ha.Name("newTodo"),
    ha.Autocomplete("off"),
  ]

  let new_input_attrs = case editing {
    Ok(_) -> new_input_base_attrs
    Error(_) -> [ha.Autofocus(""), ..new_input_base_attrs]
  }

  let todos_left = case num_todos - num_completed {
    1 -> " todo left"
    _ -> " todos left"
  }
  html.div(
    [ha.Class("todomvc-wrapper")],
    [
      html.section(
        [ha.Class("todoapp")],
        [
          html.header(
            [ha.Class("header")],
            [
              html.h1([], [html.text("todos")]),
              html.form(
                [
                  ha.Method(ha.FormPost),
                  ha.Action(routes.to_path(routes.CreateTodo)),
                ],
                [html.input(new_input_attrs)],
              ),
            ],
          ),
          html.section(
            [ha.Class("main")],
            [
              html.ul(
                [ha.Class("todo-list")],
                list.map(todos, render_todo(_, todos_route, editing)),
              ),
            ],
          ),
          html.footer(
            [ha.Class("footer")],
            [
              html.span(
                [ha.Class("todo-count")],
                [
                  html.strong(
                    [],
                    [html.text(int.to_string(num_todos - num_completed))],
                  ),
                  html.text(todos_left),
                ],
              ),
              html.ul(
                [ha.Class("filters")],
                [
                  html.li(
                    [],
                    [
                      html.a(
                        [
                          ha.class_list([
                            tuple("selected", todos_route == routes.AllTodos),
                          ]),
                          ha.Href(routes.to_path(routes.AllTodos)),
                        ],
                        [html.text("All")],
                      ),
                    ],
                  ),
                  html.li(
                    [],
                    [
                      html.a(
                        [
                          ha.class_list([
                            tuple("selected", todos_route == routes.ActiveTodos),
                          ]),
                          ha.Href(routes.to_path(routes.ActiveTodos)),
                        ],
                        [html.text("Active")],
                      ),
                    ],
                  ),
                  html.li(
                    [],
                    [
                      html.a(
                        [
                          ha.class_list([
                            tuple(
                              "selected",
                              todos_route == routes.CompleteTodos,
                            ),
                          ]),
                          ha.Href(routes.to_path(routes.CompleteTodos)),
                        ],
                        [html.text("Completed")],
                      ),
                    ],
                  ),
                ],
              ),
              html.form(
                [
                  ha.Action(routes.to_path(routes.DeleteCompletedTodos)),
                  ha.Method(ha.FormPost),
                ],
                [
                  html.button(
                    [ha.Class("clear-completed")],
                    [case num_completed {
                        0 -> html.text("")
                        _ ->
                          html.text(
                            [
                              "Clear Completed (",
                              int.to_string(num_completed),
                              ")",
                            ]
                            |> string.concat(),
                          )
                      }],
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
      html.footer(
        [ha.Class("info")],
        [
          html.p(
            [],
            [
              html.text("Written by "),
              html.a(
                [ha.Href("https://gitlab.com/greggreg/gleam_todo")],
                [html.text("GregGreg")],
              ),
            ],
          ),
        ],
      ),
    ],
  )
}

pub fn layout(inner: HTML) -> HTML {
  html.html(
    [ha.Lang("en")],
    [
      html.head(
        [],
        [
          html.meta([ha.Charset("utf-8")]),
          html.link([
            ha.Rel("shortcut icon"),
            ha.Href("/assets/favicon.ico"),
            ha.Type("image/x-icon"),
          ]),
          html.link([
            ha.Rel("icon"),
            ha.Href("/assets/favicon.ico"),
            ha.Type("image/x-icon"),
          ]),
          html.link([ha.Rel("stylesheet"), ha.Href(css_path("main"))]),
          html.title([], [html.text("Gleam Todo MVC")]),
        ],
      ),
      html.body(
        [ha.Class("learn-bar")],
        [
          html.aside(
            [ha.Class("learn")],
            [
              html.header(
                [],
                [
                  html.img([
                    ha.Id("logo"),
                    ha.Src(image_path("gleam-logo.jpg")),
                    ha.Alt("Gleam Logo"),
                  ]),
                  html.h3([], [html.text("Gleam")]),
                  html.span([], [html.h5([], [html.text("Example")])]),
                  html.a(
                    [ha.Href("https://gitlab.com/greggreg/gleam_todo")],
                    [html.text("Source")],
                  ),
                ],
              ),
              html.hr([]),
              html.blockquote(
                [ha.Class("quote speech-bubble")],
                [
                  html.p(
                    [],
                    [
                      html.text(
                        "Something here about the type of language gleam is",
                      ),
                    ],
                  ),
                  html.footer(
                    [],
                    [
                      html.a(
                        [ha.Href("https://gleam.run")],
                        [html.text("Gleam")],
                      ),
                    ],
                  ),
                ],
              ),
              html.hr([]),
              html.h4([], [html.text("Official Resources")]),
              html.ul(
                [],
                [
                  html.li(
                    [],
                    [
                      html.a(
                        [ha.Href("https://gleam.run")],
                        [html.text("Gleam Homepage")],
                      ),
                    ],
                  ),
                  html.li(
                    [],
                    [
                      html.a(
                        [ha.Href("https://github.com/gleam-lang")],
                        [html.text("Gleam on GitHub")],
                      ),
                    ],
                  ),
                ],
              ),
              html.hr([]),
            ],
          ),
          inner,
        ],
      ),
    ],
  )
}

fn render_todo(
  t: Todo,
  redirect_to: routes.Route,
  editing: Result(String, Nil),
) -> HTML {
  let is_editing = editing == Ok(t.id)
  let checked = case t.complete {
    True -> True
    False -> False
  }
  let mark_action = case t.complete {
    True -> routes.MarkActive(t.id)
    False -> routes.MarkComplete(t.id)
  }

  html.li(
    [
      ha.class_list([
        tuple("editing", is_editing),
        tuple("completed", t.complete && bool.negate(is_editing)),
      ]),
    ],
    [case is_editing {
        True ->
          html.div(
            [],
            [
              html.form(
                [
                  ha.Method(ha.FormPost),
                  ha.Action(routes.to_path(routes.EditTodo(t.id))),
                ],
                [
                  html.input([
                    ha.Type("hidden"),
                    ha.Name("redirect-to"),
                    ha.Value(routes.to_path(redirect_to)),
                  ]),
                  html.input([
                    ha.Class("edit"),
                    ha.Value(t.label),
                    ha.Autofocus(""),
                    ha.Name("label"),
                  ]),
                ],
              ),
            ],
          )
        False ->
          html.div(
            [ha.Class("view")],
            [
              html.input([
                ha.Class("toggle"),
                ha.Type("checkbox"),
                ha.Checked(checked),
              ]),
              html.label([], [html.text(t.label)]),
              html.a(
                [
                  ha.Href(routes.to_path(routes.EditTodo(t.id))),
                  ha.Class("edit-btn"),
                ],
                [html.text("✎")],
              ),
              html.form(
                [
                  ha.Method(ha.FormPost),
                  ha.Action(routes.to_path(routes.DeleteTodo(t.id))),
                ],
                [html.button([ha.Class("destroy")], [])],
              ),
              html.form(
                [
                  ha.Class("todo-mark"),
                  ha.Method(ha.FormPost),
                  ha.Action(routes.to_path(mark_action)),
                ],
                [html.button([], [])],
              ),
            ],
          )
      }],
  )
}
