import copper/connection.{Connection}
import gleam/map
import gleam/io
import gleam/option.{None, Some}
import gleam/otp/process.{Sender}
import gleam/result
import gleam/result
import gleam/string
import gleam/uri.{Uri}
import gleam_html as html
import gleam_todo_web/routes
import gleam_todo_web/todo_view
import gleam_todo/todo_store

pub type Deps {
  Deps(store: Sender(todo_store.Message), session_id: String)
}

pub fn all(conn: Connection(a), deps: Deps) -> Connection(a) {
  process.call(
    deps.store,
    todo_store.All(from: _, session_id: deps.session_id),
    1000,
  )
  |> todo_view.home(routes.AllTodos, Error(Nil))
  |> render(conn)
}

pub fn active(conn: Connection(a), deps: Deps) -> Connection(a) {
  process.call(
    deps.store,
    todo_store.Active(from: _, session_id: deps.session_id),
    1000,
  )
  |> todo_view.home(routes.ActiveTodos, Error(Nil))
  |> render(conn)
}

pub fn complete(conn: Connection(a), deps: Deps) -> Connection(a) {
  process.call(
    deps.store,
    todo_store.Complete(from: _, session_id: deps.session_id),
    1000,
  )
  |> todo_view.home(routes.CompleteTodos, Error(Nil))
  |> render(conn)
}

pub fn create(conn0: Connection(a), deps: Deps) -> Connection(a) {
  let conn = connection.load(conn0, [connection.Body])
  case connection.get_request_body(conn) {
    Ok(connection.UrlEncodedBody(params)) ->
      case map.get(map.from_list(params), "newTodo") {
        Ok(label) -> {
          process.call(
            deps.store,
            fn(f) {
              todo_store.Create(
                from: f,
                session_id: deps.session_id,
                label: label,
              )
            },
            1000,
          )
          redirect_to_previous(conn)
        }
        _ -> conn
      }
    _ -> conn
  }
}

pub fn delete(conn: Connection(a), deps: Deps, id: String) -> Connection(a) {
  process.call(
    deps.store,
    todo_store.Delete(from: _, session_id: deps.session_id, id: id),
    1000,
  )
  redirect_to_previous(conn)
}

pub fn edit(conn: Connection(a), deps: Deps, id: String) -> Connection(a) {
  let previous_route = get_previous_route(conn)
  let todo_msg = case previous_route {
    routes.ActiveTodos -> todo_store.Active(
      from: _,
      session_id: deps.session_id,
    )
    routes.CompleteTodos -> todo_store.Complete(
      from: _,
      session_id: deps.session_id,
    )
    _ -> todo_store.All(from: _, session_id: deps.session_id)
  }
  process.call(deps.store, todo_msg, 1000)
  |> todo_view.home(previous_route, Ok(id))
  |> render(conn)
}

pub fn do_edit(conn0: Connection(a), deps: Deps, id: String) -> Connection(a) {
  let conn = connection.load(conn0, [connection.Body])
  case connection.get_request_body(conn) {
    Ok(connection.UrlEncodedBody(params)) -> {
      let params = map.from_list(params)
      let previous_path =
        map.get(params, "redirect-to")
        |> result.unwrap("/")
      case map.get(params, "label") {
        Ok(label) -> {
          process.call(
            deps.store,
            fn(f) {
              todo_store.Edit(
                from: f,
                id: id,
                session_id: deps.session_id,
                label: label,
              )
            },
            1000,
          )
          connection.redirect(conn, previous_path)
        }
        _ -> conn
      }
    }
    _ -> conn
  }
}

pub fn delete_completed(conn: Connection(a), deps: Deps) -> Connection(a) {
  process.call(
    deps.store,
    todo_store.DeleteCompleted(from: _, session_id: deps.session_id),
    1000,
  )
  connection.redirect(conn, routes.to_path(routes.AllTodos))
}

pub fn mark_active(conn: Connection(a), deps: Deps, id: String) -> Connection(a) {
  mark(conn, deps, id, False)
}

pub fn mark_complete(
  conn: Connection(a),
  deps: Deps,
  id: String,
) -> Connection(a) {
  mark(conn, deps, id, True)
}

fn render(view: html.HTML, conn: Connection(a)) -> Connection(a) {
  view
  |> todo_view.layout()
  |> html.render_document()
  |> connection.put_html(conn, _)
}

fn mark(
  conn: Connection(a),
  deps: Deps,
  id: String,
  complete: Bool,
) -> Connection(a) {
  case complete {
    True ->
      process.call(
        deps.store,
        todo_store.MarkComplete(from: _, session_id: deps.session_id, id: id),
        1000,
      )
    False ->
      process.call(
        deps.store,
        todo_store.MarkActive(from: _, session_id: deps.session_id, id: id),
        1000,
      )
  }
  redirect_to_previous(conn)
}

fn redirect_to_previous(conn: Connection(a)) -> Connection(a) {
  let redirect_to = get_previous_route(conn)
  connection.redirect(conn, routes.to_path(redirect_to))
}

fn get_previous_route(conn: Connection(a)) -> routes.Route {
  let headers = connection.get_request_headers(conn)
  map.get(headers, "referer")
  |> result.then(uri.parse)
  |> result.map(fn(u: Uri) { routes.from_path(u.path) })
  |> result.unwrap(routes.AllTodos)
}
