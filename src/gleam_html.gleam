//
// TODO:
// render_pretty using StringBuilder
// 
import gleam/string
import gleam/list
import gleam/io
import gleam/string_builder.{StringBuilder}
import gleam_html/attributes.{Attribute} as ha

//
// Types
//
pub type Tag {
  Anchor
  Aside
  Blockquote
  Body
  Button
  Br
  Div
  Footer
  Form
  H1
  H3
  H4
  H5
  Head
  Header
  Hr
  Html
  Img
  Input
  Label
  Li
  Link
  Meta
  Paragraph
  Pre
  Section
  Span
  Strong
  Title
  Ul
}

pub type HTML {
  ContentNode(tag: Tag, children: List(HTML), attributes: List(Attribute))
  EmptyNode(tag: Tag, attributes: List(Attribute))
  TextNode(text: String)
}

//
// Rendering
//
fn tag_to_string(t: Tag) -> String {
  case t {
    Anchor -> "a"
    Aside -> "aside"
    Blockquote -> "blockquote"
    Body -> "body"
    Button -> "button"
    Br -> "br"
    Div -> "div"
    Footer -> "footer"
    Form -> "form"
    H1 -> "h1"
    H3 -> "h3"
    H4 -> "h4"
    H5 -> "h5"
    Head -> "head"
    Header -> "header"
    Hr -> "hr"
    Html -> "html"
    Img -> "img"
    Input -> "input"
    Label -> "label"
    Li -> "li"
    Link -> "link"
    Meta -> "meta"
    Paragraph -> "p"
    Pre -> "pre"
    Section -> "section"
    Span -> "span"
    Strong -> "strong"
    Title -> "title"
    Ul -> "ul"
  }
}

pub fn render(h: HTML) -> String {
  render_help(h)
  |> string_builder.to_string
}

fn escape(in: String) -> StringBuilder {
  in
  |> string.to_graphemes()
  |> list.map(fn(g) {
    case g {
      "'" -> "&apos;"
      "\"" -> "&quot;"
      "&" -> "&amp;"
      "<" -> "&lt;"
      ">" -> "&gt;"

      _ -> g
    }
  })
  |> string_builder.from_strings()
}

pub fn render_help(h: HTML) -> StringBuilder {
  case h {
    TextNode(text: text) -> escape(text)
    EmptyNode(tag: t, attributes: a) ->
      ["<", tag_to_string(t), ha.render(a), "/>"]
      |> string_builder.from_strings()
    ContentNode(tag: t, children: c, attributes: a) -> {
      let left =
        ["<", tag_to_string(t), ha.render(a), ">"]
        |> string_builder.from_strings()
      let middle = list.map(c, render_help)
      let right =
        ["</", tag_to_string(t), ">"]
        |> string_builder.from_strings()
      [[left], middle, [right]]
      |> list.flatten
      |> string_builder.concat()
    }
  }
}

const indent: String = "    "

pub fn render_document_pretty(h: HTML) -> String {
  ["<!DOCTYPE html>\n", render_pretty(h)]
  |> string.concat()
}

pub fn render_pretty(h: HTML) -> String {
  do_render_pretty(h, [])
  |> io.debug()
  |> string.join("\n")
}

fn list_wrap(a: a) -> List(a) {
  [a]
}

fn do_render_pretty(h: HTML, i: List(String)) -> List(String) {
  case h {
    TextNode(text: text) ->
      [i, [string_builder.to_string(escape(text))]]
      |> list.flatten()
      |> string.concat()
      |> list_wrap()

    EmptyNode(..) ->
      [render(h), ..i]
      |> list.reverse()
      |> string.concat()
      |> list_wrap()

    ContentNode(tag: t, children: c, attributes: a) -> {
      let top =
        [i, ["<", tag_to_string(t), ha.render(a), ">"]]
        |> list.flatten()
        |> string.concat()
        |> list_wrap()
      let bottom =
        [i, ["</", tag_to_string(t), ">"]]
        |> list.flatten()
        |> string.concat()
        |> list_wrap()
      let middle = case t {
        Pre -> list.map(c, render)
        _ ->
          list.map(c, do_render_pretty(_, [indent, ..i]))
          |> list.flatten()
      }
      [top, middle, bottom]
      |> list.flatten()
    }
  }
}

pub fn render_document(h: HTML) -> String {
  ["<!DOCTYPE html>", render(h)]
  |> string.concat()
}

//
// Structure
//
pub fn a(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Anchor, children: c, attributes: a)
}

pub fn aside(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Aside, children: c, attributes: a)
}

pub fn blockquote(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Blockquote, children: c, attributes: a)
}

pub fn body(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Body, children: c, attributes: a)
}

pub fn br(a: List(Attribute)) -> HTML {
  EmptyNode(tag: Br, attributes: a)
}

pub fn button(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Button, children: c, attributes: a)
}

pub fn div(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Div, children: c, attributes: a)
}

pub fn footer(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Footer, children: c, attributes: a)
}

pub fn form(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Form, children: c, attributes: a)
}

pub fn h1(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: H1, children: c, attributes: a)
}

pub fn h3(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: H3, children: c, attributes: a)
}

pub fn h4(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: H4, children: c, attributes: a)
}

pub fn h5(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: H5, children: c, attributes: a)
}

pub fn head(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Head, children: c, attributes: a)
}

pub fn header(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Header, children: c, attributes: a)
}

pub fn hr(a: List(Attribute)) -> HTML {
  EmptyNode(tag: Hr, attributes: a)
}

pub fn html(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Html, children: c, attributes: a)
}

pub fn img(a: List(Attribute)) -> HTML {
  EmptyNode(tag: Img, attributes: a)
}

pub fn input(a: List(Attribute)) -> HTML {
  EmptyNode(tag: Input, attributes: a)
}

pub fn label(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Label, children: c, attributes: a)
}

pub fn li(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Li, children: c, attributes: a)
}

pub fn link(a: List(Attribute)) -> HTML {
  EmptyNode(tag: Link, attributes: a)
}

pub fn meta(a: List(Attribute)) -> HTML {
  EmptyNode(tag: Meta, attributes: a)
}

/// Be careful what you make a child of a Paragraph,
/// they can only contain block level elements.
/// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p
pub fn p(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Paragraph, children: c, attributes: a)
}

pub fn pre(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Pre, children: c, attributes: a)
}

pub fn section(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Section, children: c, attributes: a)
}

pub fn span(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Span, children: c, attributes: a)
}

pub fn strong(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Strong, children: c, attributes: a)
}

pub fn title(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Title, children: c, attributes: a)
}

pub fn text(text: String) -> HTML {
  TextNode(text: text)
}

pub fn ul(a: List(Attribute), c: List(HTML)) -> HTML {
  ContentNode(tag: Ul, children: c, attributes: a)
}
