import gleam/io

/// Debug helper 
pub fn d(label: String, val: a) -> Nil {
  io.debug(tuple(label, val))
  Nil
}
