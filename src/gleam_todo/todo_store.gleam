import gleam/io
import gleam/list
import gleam/otp/actor.{Continue}
import gleam/otp/process.{Sender}
import gleam/string
import gleam/map.{Map}
import gleam/result
import gleam_uuid

const day_in_ms = 86_400_000

pub type Todo {
  Todo(complete: Bool, label: String, id: String)
}

pub type State {
  State(todos: Map(String, List(Todo)))
}

pub fn initial_state() {
  State(todos: map.new())
}

fn default_todos() -> List(Todo) {
  [
    Todo(complete: False, label: "Checkout Gleam", id: gleam_uuid.v4_string()),
    Todo(complete: False, label: "Write some code", id: gleam_uuid.v4_string()),
    Todo(complete: True, label: "???", id: gleam_uuid.v4_string()),
    Todo(complete: False, label: "Profit.", id: gleam_uuid.v4_string()),
  ]
}

pub type Message {
  Active(from: Sender(List(Todo)), session_id: String)
  All(from: Sender(List(Todo)), session_id: String)
  Complete(from: Sender(List(Todo)), session_id: String)
  Create(from: Sender(List(Todo)), session_id: String, label: String)
  Delete(from: Sender(List(Todo)), session_id: String, id: String)
  DeleteCompleted(from: Sender(List(Todo)), session_id: String)
  Edit(from: Sender(List(Todo)), id: String, session_id: String, label: String)
  EnsureTodos(from: Sender(Bool), session_id: String)
  MarkActive(from: Sender(List(Todo)), session_id: String, id: String)
  MarkComplete(from: Sender(List(Todo)), session_id: String, id: String)
  ResetAllTodos(from: Sender(Nil), self: Sender(Message))
}

fn todos_for_session(
  todos: Map(String, List(Todo)),
  session_id: String,
) -> List(Todo) {
  map.get(todos, session_id)
  |> result.unwrap([])
}

pub fn handle_message(msg: Message, state: State) {
  case msg {
    Active(from: from, session_id: session_id) -> {
      state.todos
      |> todos_for_session(session_id)
      |> list.filter(fn(t: Todo) { t.complete == False })
      |> process.send(from, _)
      Continue(state)
    }
    All(from: from, session_id: session_id) -> {
      state.todos
      |> todos_for_session(session_id)
      |> process.send(from, _)
      Continue(state)
    }
    Create(from: from, session_id: session_id, label: label) -> {
      let new =
        Todo(
          complete: False,
          label: string.slice(label, 0, 150),
          id: gleam_uuid.v4_string(),
        )
      let session_todos = [new, ..todos_for_session(state.todos, session_id)]
      process.send(from, session_todos)
      let todos = map.insert(state.todos, session_id, session_todos)
      Continue(State(todos: todos))
    }
    Complete(from: from, session_id: session_id) -> {
      state.todos
      |> todos_for_session(session_id)
      |> list.filter(fn(t: Todo) { t.complete == True })
      |> process.send(from, _)
      Continue(state)
    }
    Delete(from: from, session_id: session_id, id: id) -> {
      let kept =
        state.todos
        |> todos_for_session(session_id)
        |> list.filter(fn(t: Todo) { t.id != id })
      process.send(from, kept)
      let todos = map.insert(state.todos, session_id, kept)
      Continue(State(todos: todos))
    }
    DeleteCompleted(from: from, session_id: session_id) -> {
      let active =
        state.todos
        |> todos_for_session(session_id)
        |> list.filter(fn(t: Todo) { t.complete == False })
      process.send(from, active)
      let todos = map.insert(state.todos, session_id, active)
      Continue(State(todos: todos))
    }
    Edit(from: from, id: id, session_id: session_id, label: label) -> {
      let session_todos =
        state.todos
        |> todos_for_session(session_id)
        |> list.map(fn(t: Todo) {
          case t.id == id {
            False -> t
            True -> Todo(..t, label: label)
          }
        })
      process.send(from, session_todos)
      let todos = map.insert(state.todos, session_id, session_todos)
      Continue(State(todos: todos))
    }
    EnsureTodos(from: from, session_id: session_id) -> {
      let todos = case map.get(state.todos, session_id) {
        Ok(_) -> state.todos
        Error(_) -> map.insert(state.todos, session_id, default_todos())
      }
      process.send(from, True)
      Continue(State(todos: todos))
    }
    MarkActive(from: from, session_id: session_id, id: id) -> {
      let updated =
        state.todos
        |> todos_for_session(session_id)
        |> mark(id, False)
      process.send(from, updated)
      let todos = map.insert(state.todos, session_id, updated)
      Continue(State(todos: todos))
    }
    MarkComplete(from: from, session_id: session_id, id: id) -> {
      let updated =
        state.todos
        |> todos_for_session(session_id)
        |> mark(id, True)
      process.send(from, updated)
      let todos = map.insert(state.todos, session_id, updated)
      Continue(State(todos: todos))
    }
    ResetAllTodos(from: from, self: self) -> {
      // TODO: I don't know if i'm doing this right
      io.debug("Todo Store: Resetting all Todos")
      process.send(from, Nil)
      process.send_after(self, day_in_ms, ResetAllTodos(from: from, self: self))
      Continue(State(todos: map.new()))
    }
  }
}

fn mark(todos: List(Todo), id: String, complete: Bool) {
  todos
  |> list.map(fn(t: Todo) {
    case t.id {
      x if x == id -> Todo(..t, complete: complete)
      _ -> t
    }
  })
}
