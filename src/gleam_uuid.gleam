//// UUID V4
////
////
//// ### Usage
//// ```gleam
//// import gleam_uuid
////
////
//// let uuid = gleam_uuid.v4()
//// ```
//// TODO: correctness tests, variant parsing, more versions, perf test

import gleam/bit_builder.{BitBuilder}
import gleam/bit_string.{BitString}
import gleam/list
import gleam/string

pub type UUIDVersion {
  V4
}

pub opaque type UUID {
  UUID(value: BitString, version: UUIDVersion)
}

/// Generates a version 4 (random) UUID.
/// https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_(random)
pub fn v4() -> UUID {
  let version = 4
  let variant = 8

  assert <<a:size(48), b:size(12), c:size(60)>> = strong_rand_bytes(15)

  let value = <<
    a:size(48),
    version:size(4),
    b:size(12),
    variant:size(4),
    c:size(60),
  >>

  UUID(value: value, version: V4)
}

pub fn to_string(uuid: UUID) -> String {
  case uuid.version {
    V4 -> do_v4_to_string(uuid.value, 0, [])
  }
}

pub fn v4_string() -> String {
  v4()
  |> to_string()
}

fn do_v4_to_string(ints: BitString, position: Int, acc: List(String)) -> String {
  case position {
    8 | 13 | 18 | 23 -> do_v4_to_string(ints, position + 1, ["-", ..acc])
    _ ->
      case ints {
        <<i:size(4), rest:bit_string>> ->
          do_v4_to_string(rest, position + 1, [int_to_hex(i), ..acc])
        <<>> ->
          acc
          |> list.reverse()
          |> string.concat()
      }
  }
}

pub fn from_string(in: String) -> Result(UUID, Nil) {
  in
  |> string.to_graphemes()
  |> from_string_help(0, bit_builder.from_bit_string(<<>>))
}

fn from_string_help(
  chars: List(String),
  position: Int,
  acc: BitBuilder,
) -> Result(UUID, Nil) {
  case tuple(position, chars) {
    tuple(32, []) ->
      Ok(UUID(value: bit_builder.to_bit_string(acc), version: V4))
    tuple(p, _) if p > 32 -> Error(Nil)
    tuple(_, ["-", ..rest]) -> from_string_help(rest, position, acc)
    tuple(12, [x, ..]) if x != "4" -> Error(Nil)
    tuple(_, [c, ..rest]) ->
      case hex_to_int(c) {
        Ok(i) ->
          from_string_help(
            rest,
            position + 1,
            bit_builder.append(acc, <<i:size(4)>>),
          )
        _ -> Error(Nil)
      }
  }
}

// Hex Helpers
fn hex_to_int(c: String) -> Result(Int, Nil) {
  let i = case c {
    "0" -> 0
    "1" -> 1
    "2" -> 2
    "3" -> 3
    "4" -> 4
    "5" -> 5
    "6" -> 6
    "7" -> 7
    "8" -> 8
    "9" -> 9
    "a" | "A" -> 10
    "b" | "B" -> 11
    "c" | "C" -> 12
    "d" | "D" -> 13
    "e" | "E" -> 14
    "f" | "F" -> 15
    _ -> 16
  }
  case i {
    16 -> Error(Nil)
    x -> Ok(x)
  }
}

fn int_to_hex(int: Int) -> String {
  integer_to_binary(int, 16)
}

// Erlang Bridge
external fn strong_rand_bytes(Int) -> String =
  "crypto" "strong_rand_bytes"

external fn integer_to_binary(Int, Int) -> String =
  "erlang" "integer_to_binary"
