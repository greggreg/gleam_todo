////
//// Copper :
////     A specification and conveniences for composable functions between web applications
////     A shameless rip-off of plug: https://hex.pm/packages/plug
////
//// `pipe_thru` is the main interface.
//// see todo_web/router for an example.
////

import copper/connection.{Connection}
import copper/final_response.{FinalResponse}
import copper/status_code
import gleam/dynamic.{Dynamic}
import gleam/int
import gleam/io
import gleam/option.{None}
import gleam/otp/actor.{StartError}
import gleam/otp/process.{Pid, Sender}
import gleam/otp/supervisor
import gleam/string

// Configuration needed to work with Copper.
// See todo_application.gleam for an example
pub type Config(init_result, raw_request) {
  Config(
    port: Int,
    on_init: fn() -> init_result,
    root_dispatch: fn(Connection(raw_request), init_result) ->
      Connection(raw_request),
    server_interface: connection.ServerInterface(raw_request),
  )
}

// Worker function for starting in a supervision tree
pub fn start(
  _args: Nil,
  config: Config(init_result, raw_request),
  start_server: fn(
    Int,
    tuple(
      fn(raw_request, Config(init_result, raw_request), init_result) ->
        FinalResponse,
      Config(init_result, raw_request),
      init_result,
    ),
  ) ->
    Result(Pid, Dynamic),
) -> Result(Sender(init_result), StartError) {
  let init_result = config.on_init()

  io.debug(string.concat([
    "Starting Server on port: ",
    int.to_string(config.port),
  ]))

  start_server(config.port, tuple(request_handler, config, init_result))
  |> supervisor.from_erlang_start_result()
}

// Function all requests start in
fn request_handler(
  cowboy_request: raw_request,
  config: Config(init_result, raw_request),
  init_result: init_result,
) -> FinalResponse {
  [
    connection.debug_request(
      _,
      [
        connection.Method,
        connection.Path,
        connection.Query,
        connection.Cookies,
        connection.Headers,
      ],
    ),
    config.root_dispatch(_, init_result),
    or_else_404(_),
  ]
  |> pipe_thru(connection.new(cowboy_request, config.server_interface))
  |> final_response.from_connection()
  |> final_response.debug()
}

fn or_else_404(conn: Connection(raw_request)) -> Connection(raw_request) {
  case connection.get_response_status_code(conn) {
    None -> connection.put_response_status_code(conn, status_code.NotFound)
    _ -> conn
  }
}

/// This is the main public interface
/// Takes init_result pipeline and a connection and runs the connection through the pipeline
pub fn pipe_thru(
  pipeline: List(fn(Connection(a)) -> Connection(a)),
  conn: Connection(a),
) -> Connection(a) {
  case pipeline {
    [] -> conn
    [fun, ..rest] ->
      case connection.get_condition(conn) {
        connection.Normal -> {
          let conn1 = fun(conn)
          pipe_thru(rest, conn1)
        }
        connection.Halted -> conn
      }
  }
}
