import copper/connection.{Connection}
import copper/headers
import copper/mime_types
import copper/status_code
import gleam/io
import gleam/map.{Map}
import gleam/option.{None, Some}
import gleam/string
import gleam_todo/utils.{d}

pub type FinalResponse {
  FinalResponse(
    status_code: Int,
    body: String,
    headers: Map(String, String),
    cookies: Map(String, String),
  )
}

pub fn from_connection(conn: Connection(a)) -> FinalResponse {
  case connection.get_response_status_code(conn) {
    Some(code) -> {
      let body =
        conn
        |> connection.get_response_body()
        |> option.unwrap(or: "")
      FinalResponse(
        status_code: status_code.to_int(code),
        body: body,
        headers: conn
        |> connection.get_response_headers()
        |> headers.to_map(),
        cookies: conn
        |> connection.get_response_cookies(),
      )
    }
    None -> error_response("No response status code set.")
  }
}

fn error_response(message: String) -> FinalResponse {
  FinalResponse(
    status_code: status_code.to_int(status_code.InternalServerError),
    body: string.concat(["Internal Server Error: ", message]),
    headers: headers.to_map([headers.ContentType(mime_types.PlainText)]),
    cookies: map.new(),
  )
}

pub fn debug(resp: FinalResponse) -> FinalResponse {
  io.debug("|-Copper-Response-Debug-----------------------|")
  d("Status code", resp.status_code)
  d("Headers", resp.headers)
  d("Cookies", resp.cookies)
  io.debug("|--------------------------------------------|")
  resp
}
