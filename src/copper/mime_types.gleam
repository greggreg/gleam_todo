pub type MimeType {
  Html
  PlainText
}

pub fn to_string(mime: MimeType) {
  case mime {
    Html -> "text/html"
    PlainText -> "text/plain"
  }
}
