////
//// The datatype that flows through pipelines in copper
////

import copper/headers.{Header}
import copper/mime_types
import copper/status_code.{StatusCode}
import gleam/atom.{Atom}
import gleam/io
import gleam/list
import gleam/map.{Map}
import gleam/option.{None, Option, Some}
import gleam/string
import gleam_todo/utils.{d}

pub type ServerInterface(a) {
  ServerInterface(
    get_request_cookies: fn(a) -> List(tuple(String, String)),
    get_request_path: fn(a) -> String,
    get_request_query: fn(a) -> String,
    get_request_scheme: fn(a) -> String,
    get_request_method: fn(a) -> String,
    get_request_headers: fn(a) -> Map(String, String),
    get_request_string_body: fn(a) -> tuple(Atom, String, a),
    get_request_url_encoded_body: fn(a) ->
      tuple(Atom, List(tuple(String, String)), a),
  )
}

pub opaque type Connection(a) {
  Connection(
    response: Response,
    raw_request: a,
    request: Request,
    data: Map(String, String),
    condition: ConnectionCondition,
    server_interface: ServerInterface(a),
  )
}

type Request {
  Request(
    body: Result(RequestBodyType, RequestBodyStatus),
    cookies: Option(Map(String, String)),
    headers: Option(Map(String, String)),
    method: Option(RequestMethod),
    scheme: Option(Scheme),
  )
}

pub type ConnectionCondition {
  Normal
  Halted
}

pub type RequestBodyStatus {
  BodyNotFetched
}

pub type RequestBodyType {
  StringBody(String)
  // TODO: values can be booleans as well
  UrlEncodedBody(List(tuple(String, String)))
}

pub type Scheme {
  Http
  Https
}

pub type RequestMethod {
  Connect
  Delete
  Get
  Head
  Options
  Patch
  Post
  Put
  Trace
}

pub type RequestPart {
  Body
  Cookies
  Headers
  Method
  Path
  Query
  Scheme
}

pub opaque type Response {
  Response(
    cookies: Map(String, String),
    status_code: Option(StatusCode),
    body: Option(String),
    headers: List(Header),
  )
}

fn new_request() -> Request {
  Request(
    body: Error(BodyNotFetched),
    cookies: None,
    scheme: None,
    method: None,
    headers: None,
  )
}

pub fn new(
  raw_request: a,
  server_interface: ServerInterface(a),
) -> Connection(a) {
  Connection(
    raw_request: raw_request,
    request: new_request(),
    response: Response(
      cookies: map.new(),
      status_code: None,
      body: None,
      headers: [],
    ),
    data: map.new(),
    condition: Normal,
    server_interface: server_interface,
  )
}

pub fn response(conn: Connection(a)) -> Response {
  conn.response
}

//
// Data Interface
//
pub fn get_data(conn: Connection(a), key: String) -> Result(String, Nil) {
  conn.data
  |> map.get(key)
}

pub fn put_data(conn: Connection(a), key: String, val: String) -> Connection(a) {
  let data =
    conn.data
    |> map.insert(key, val)
  Connection(..conn, data: data)
}

pub fn get_data_key(conn: Connection(a), key: String) -> Result(String, Nil) {
  conn.data
  |> map.get(key)
}

//
// Request Interface
//
pub fn load(conn: Connection(a), parts: List(RequestPart)) -> Connection(a) {
  case parts {
    [] -> conn
    [part, ..rest] -> load(load_part(conn, part), rest)
  }
}

fn load_part(conn: Connection(a), part: RequestPart) -> Connection(a) {
  let conn_request = conn.request
  case part {
    Body -> {
      // TODO: There are other kinds of bodies, including no body
      let h = get_request_headers(conn)
      case map.get(h, "content-type") {
        Ok("application/x-www-form-urlencoded") -> {
          let tuple(req, body) =
            do_get_request_url_encoded_body(conn.raw_request, conn)
          Connection(
            ..conn,
            raw_request: req,
            request: Request(..conn_request, body: Ok(UrlEncodedBody(body))),
          )
        }
        _ -> {
          let tuple(req, body) =
            do_get_request_string_body(
              conn.raw_request,
              conn.server_interface.get_request_string_body,
              [],
            )
          Connection(
            ..conn,
            raw_request: req,
            request: Request(..conn_request, body: Ok(StringBody(body))),
          )
        }
      }
    }
    Cookies -> {
      let c = get_request_cookies(conn)
      Connection(..conn, request: Request(..conn_request, cookies: Some(c)))
    }

    Headers -> {
      let h = get_request_headers(conn)
      Connection(..conn, request: Request(..conn_request, headers: Some(h)))
    }
    Method -> {
      let m = get_request_method(conn)
      Connection(..conn, request: Request(..conn_request, method: Some(m)))
    }
    Path -> conn
    Query -> conn
    Scheme -> {
      let s = get_request_scheme(conn)
      Connection(..conn, request: Request(..conn_request, scheme: Some(s)))
    }
  }
}

// COOKIES
pub fn get_request_cookies(conn: Connection(a)) -> Map(String, String) {
  case conn.request.cookies {
    None ->
      conn.server_interface.get_request_cookies(conn.raw_request)
      |> map.from_list()
    Some(cookies) -> cookies
  }
}

// PATH
pub fn get_request_path(conn: Connection(a)) -> String {
  conn.server_interface.get_request_path(conn.raw_request)
}

// QUERY
pub fn get_request_query(conn: Connection(a)) -> String {
  conn.server_interface.get_request_query(conn.raw_request)
}

// SCHEME
pub fn get_request_scheme(conn: Connection(a)) -> Scheme {
  case conn.request.scheme {
    None -> {
      let s = case conn.server_interface.get_request_scheme(conn.raw_request) {
        "https" -> Https
        _ -> Http
      }
      s
    }
    Some(scheme) -> scheme
  }
}

// METHOD
pub fn get_request_method(conn: Connection(a)) -> RequestMethod {
  case conn.request.method {
    None -> {
      let m = case conn.server_interface.get_request_method(conn.raw_request) {
        "CONNECT" -> Connect
        "DELETE" -> Delete
        "GET" -> Get
        "HEAD" -> Head
        "OPTIONS" -> Options
        "PATCH" -> Patch
        "POST" -> Post
        "PUT" -> Put
        _ -> Trace
      }
      m
    }
    Some(method) -> method
  }
}

// HEADERS
pub fn get_request_headers(conn: Connection(a)) -> Map(String, String) {
  case conn.request.headers {
    None -> conn.server_interface.get_request_headers(conn.raw_request)
    Some(headers) -> headers
  }
}

// BODY
pub fn get_request_body(
  conn: Connection(a),
) -> Result(RequestBodyType, RequestBodyStatus) {
  conn.request.body
}

// TODO, this has cowboy specific code, move it out to copper_cowboy
fn do_get_request_string_body(
  req: a,
  raw_get_string_body: fn(a) -> tuple(Atom, String, a),
  acc: List(String),
) -> tuple(a, String) {
  let cowboy_ok: Atom = atom.create_from_string("ok")
  let cowboy_more: Atom = atom.create_from_string("more")
  case raw_get_string_body(req) {
    tuple(ok, data, new_req) if ok == cowboy_ok -> tuple(
      new_req,
      [data, ..acc]
      |> list.reverse()
      |> string.concat(),
    )

    tuple(more, data, new_req) if more == cowboy_more ->
      do_get_request_string_body(new_req, raw_get_string_body, [data, ..acc])

    // TODO: this should never happen?
    // further reading: https://ninenines.eu/docs/en/cowboy/2.6/manual/cowboy_req.read_body/
    _ -> tuple(req, string.concat(list.reverse(acc)))
  }
}

fn do_get_request_url_encoded_body(
  req: a,
  conn: Connection(a),
) -> tuple(a, List(tuple(String, String))) {
  let cowboy_ok: Atom = atom.create_from_string("ok")
  case conn.server_interface.get_request_url_encoded_body(req) {
    tuple(ok, body, new_req) if ok == cowboy_ok -> tuple(new_req, body)

    // TODO: This should never happen?
    // further reading: https://ninenines.eu/docs/en/cowboy/2.6/manual/cowboy_req.read_urlencoded_body/
    _ -> tuple(req, [])
  }
}

//
// Response Interface
//
// BODY
pub fn put_response_body(conn: Connection(a), body: String) -> Connection(a) {
  let resp = conn.response
  Connection(..conn, response: Response(..resp, body: Some(body)))
}

pub fn get_response_body(conn: Connection(a)) -> Option(String) {
  conn.response.body
}

// COOKIES
pub fn put_response_cookie(
  conn: Connection(a),
  key: String,
  val: String,
) -> Connection(a) {
  let resp = conn.response
  Connection(
    ..conn,
    response: Response(..resp, cookies: map.insert(resp.cookies, key, val)),
  )
}

pub fn get_response_cookies(conn: Connection(a)) -> Map(String, String) {
  conn.response.cookies
}

// STATUS CODE
pub fn put_response_status_code(
  conn: Connection(a),
  code: StatusCode,
) -> Connection(a) {
  let resp = conn.response
  Connection(..conn, response: Response(..resp, status_code: Some(code)))
}

pub fn get_response_status_code(conn: Connection(a)) -> Option(StatusCode) {
  conn.response.status_code
}

// HEADERS
pub fn put_response_headers(
  conn: Connection(a),
  headers: List(Header),
) -> Connection(a) {
  let resp = conn.response
  Connection(..conn, response: Response(..resp, headers: headers))
}

pub fn add_response_header(conn: Connection(a), header: Header) -> Connection(a) {
  let old_headers = get_response_headers(conn)
  let resp = conn.response
  Connection(
    ..conn,
    response: Response(..resp, headers: [header, ..old_headers]),
  )
}

pub fn get_response_headers(conn: Connection(a)) -> List(Header) {
  conn.response.headers
}

pub fn get_condition(conn: Connection(a)) -> ConnectionCondition {
  conn.condition
}

//
// Response Conveniences
//
pub fn put_html(conn: Connection(a), body: String) -> Connection(a) {
  conn
  |> add_response_header(headers.ContentType(mime_types.Html))
  |> put_response_body(body)
}

pub fn redirect(conn: Connection(a), location: String) -> Connection(a) {
  conn
  |> put_response_status_code(status_code.SeeOther)
  |> add_response_header(headers.Location(location))
}

pub fn halt(conn: Connection(a)) -> Connection(a) {
  Connection(..conn, condition: Halted)
}

//
// LOG CONVENIENCES
//
pub fn debug_request(
  conn0: Connection(a),
  parts: List(RequestPart),
) -> Connection(a) {
  let conn = load(conn0, parts)
  io.debug("|-Copper-Request-Debug-----------------------|")
  parts
  |> list.map(fn(p) {
    case p {
      Body -> d("Body", "no support for logging request body yet")
      Cookies -> d("Cookies", get_request_cookies(conn))
      Headers -> d("Headers", get_request_headers(conn))
      Method -> d("Method", get_request_method(conn))
      Path -> d("Path", get_request_path(conn))
      Query -> d("Query", get_request_query(conn))
      Scheme -> d("Scheme", get_request_scheme(conn))
    }
  })
  io.debug("|--------------------------------------------|")

  conn
}
