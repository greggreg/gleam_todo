import copper/mime_types.{MimeType}
import gleam/map.{Map}
import gleam/list

pub type Header {
  ContentType(MimeType)
  Location(String)
}

pub fn to_map(headers: List(Header)) -> Map(String, String) {
  headers
  |> list.map(to_tuple)
  |> map.from_list()
}

pub fn to_tuple(header: Header) -> tuple(String, String) {
  case header {
    ContentType(mime) -> tuple("content-type", mime_types.to_string(mime))
    Location(l) -> tuple("location", l)
  }
}
