import copper
import copper_cowboy
import gleam/dynamic.{Dynamic}
import gleam/int
import gleam/io
import gleam/map
import gleam/os
import gleam/otp/actor
import gleam/otp/process.{Sender}
import gleam/otp/supervisor.{ApplicationStartMode, ErlangStartResult}
import gleam/result
import gleam_todo/todo_store
import gleam_todo_web/router

//
// OTP INTERFACE
//
// OTP init
fn init(children) {
  children
  |> supervisor.add(supervisor.worker(copper.start(
    _,
    copper_config(),
    copper_cowboy.cowboy_start_link,
  )))
}

// OTP Start
pub fn start(
  _mode: ApplicationStartMode,
  _args: List(Dynamic),
) -> ErlangStartResult {
  init
  |> supervisor.start
  |> supervisor.to_erlang_start_result
}

// OTP Stop
pub fn stop(_state: Dynamic) {
  supervisor.application_stopped()
}

//
// APPLICATION CONFIGURATION
//
fn copper_config() -> copper.Config(
  Sender(todo_store.Message),
  copper_cowboy.CowboyRequest,
) {
  let port =
    os.get_env()
    |> map.get("GLEAM_TODO_PORT")
    |> result.then(int.parse)
    |> result.unwrap(8000)
  copper.Config(
    port: port,
    on_init: on_copper_init,
    root_dispatch: router.pipeline,
    server_interface: copper_cowboy.server_interface(),
  )
}

// Callback for right before the server starts
fn on_copper_init() -> Sender(todo_store.Message) {
  assert Ok(store) =
    actor.start(todo_store.initial_state(), todo_store.handle_message)
  process.call(store, todo_store.ResetAllTodos(from: _, self: store), 1000)
  store
}
