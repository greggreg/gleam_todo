# Gleam TODO

A serverside only implementation of [Todo MVC](http://todomvc.com/) written in [Gleam](https://gleam.run)!

Demo: http://gleam-todo.greg.lol

## Why
* To play around with Gleam as a language
* To experiment with writing a [plug](https://github.com/elixir-plug/plug) like HTTP connection handler.
* To experiment with Gleam's OTP, see: [todo_store](https://gitlab.com/greggreg/gleam_todo/-/blob/master/src/gleam_todo/todo_store.gleam)

## Possibly interesting parts
copper : A library for turning an HTTP request into a pipeline of functions that take and return a `copper.Connection` very much inspired by Elixir's plug

copper_cowboy : An adapter for using copper on top of erlang's Cowboy webserver

gleam_html : A library for building HTML pages inspired by elm's html library

todo_store : A Gleam OTP Agent for storing user's todos

gleam_uuid : A module for generating, decoding, and verifying v4 UUIDs

## Running Locally
You are going to need:
* [Erlang](https://github.com/erlang/otp/releases)
* [Rebar3](https://rebar3.org/docs/getting-started/)
* [Gleam](https://gleam.run/getting-started/#installing-gleam)

```sh
# Clone this repo
> git clone http://gitlab.com/greggreg/gleam_todo

# cd into the project
> cd gleam_todo

# Build the project
> rebar3 compile

# Run the Erlang REPL
> rebar3 shell
```

It should now be running on port 8000.


To run on a different port, set `GLEAM_TODO_PORT` in your environment:
```
GLEAM_TODO_PORT=4567 rebar3 shell
```

## Building a release and deploying
1) Build the tarball
```sh
# from the project directory
> rebar3 as prod tar
...
===> Building release tarball gleam_todo-0.0.1.tar.gz...
===> Tarball successfully created: _build/prod/rel/gleam_todo/gleam_todo-x.x.x.tar.gz
```

2) Copy the tarball to a server with Erlang installed
3) Decompress the tarball
```sh
> tar -zxvf gleam_todo-x.x.x.tar.gz
```
4) Run the application in the background on port 80
```sh
> sudo GLEAM_TODO_PORT=80 ./bin/gleam_todo daemon
```

