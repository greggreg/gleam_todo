{mod, {myapp_app, []}} in myapp.app.src specfies the module and args to call

this module is expected to return the PID of a supervision tree

the module needs start(type, args) and stop(state) functions



## Gleam Features I would like:
* better tuple syntax, perhaps just shorten `tuple` to `t`: `t(x, y, z)`
* Option(a) an alias for Result(a, Nil), Result(_, Nil) is a good enough N+1 type, but Option conveys intent more precicesly and quickly
* exhaustive pattern match checking
* lambda type inference, gleam knows what type `state.todos` is, but it still requires the type for `t` in the lambda
```
      state.todos
      |> list.filter(fn(t: Todo) { t.id != id })
```
* nicer looking debug logging, remove the erlang brackets, supporting color would be awesome, 
* a debug module with things like `debug.inspect/2` that takes a label or something
* unicode support in debug logging
```
> gleam@io:debug(<<"👍"/utf8>>).
<<240,159,145,141>>
```
* import all? `import something.{..}`
* bug finding in message passing can be tough
* unused module import warning
* is there a process cast?
* result.or take a function as a second argument
* result.unwrap take a function as a second argument
* todo produces big distracting warnings, makes refactoring hard
* process.send can cause a crash if it is replys with the wrong type or forgets to reply with no compiler checking
* I'm not sure this warning is useful, it prevents writing general code
```
warning: Redundant record update
   ┌─ /Users/greg/Trash/gleam_todo/src/todos/todo_store.gleam:78:16
   │
78 │       Continue(State(..state, todos: todos))
   │                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^

All fields have been given in this record update, so it does not need to be
be an update. Remove the update or remove fields that need to be copied.
Done!
```
* it would be nice to have a `gleam build --watch` option so I didn't have to use `rebar3 auto` and deal with all the extra rebar output
* char to int syntax erlang: `$A` elixir `?A`

## std lib:
* map.new() should probably be map.empty()
* map.put() would be nicer than map.insert()
* bitstring is a confusing type name, implies that it as a bit representation of a String, `bits` is probably better

## Formatter:
* sort imports alphabetically
* correct `fn ()` to `fn()`
* a `breathy` option that spreads code out more. Makes diffs more representative of changes and easier to read.
```
let a =
    whatever

let a = 
   whatever
   |> function()

case x {
  _ ->
    whatever

  _ -> {
    let a =
      whatever
 
    a
  }
}

fn whatever(
  all_types: OnTheirOwnLine
) -> ReturnType
{
  let a =
    whatever

  a
}

```
