import gleam_html
import gleam/should
import gleam_html as html

pub fn render_document_test() {
  html.render_document(html.div([], [html.text("hello")]))
  |> should.equal("<!DOCTYPE html><div>hello</div>")
}

pub fn render_ul_test() {
  html.ul([], [html.li([], [html.text("item")])])
  |> html.render()
  |> should.equal("<ul><li>item</li></ul>")
}

pub fn render_pretty_test() {
  html.div([], [html.p([], [html.text("hi")])])
  |> html.render_pretty()
  |> should.equal("<div>
    <p>
        hi
    </p>
</div>")
}
